sudo apt-get update
sudo apt-get upgrade -y
sudo apt install -y wget openssh-server ca-certificates make gcc
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
make
sudo make install
# redis-server &

### more stuff

sudo mkdir /etc/redis
sudo mkdir /var/redis

sudo cp ~/redis-stable/utils/redis_init_script /etc/init.d/redis_6379
sudo cp ~/redis-stable/redis.conf /etc/redis/6379.conf

sudo mkdir /var/redis/6379


sudo update-rc.d redis_6379 defaults

# sudo /etc/init.d/redis_6379 start
