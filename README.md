# Packer AMI builder

# Project Title

A collection of Packer repos for the creation of Amazon Machine Images (AMIs) on AWS.

AMIs list:
* GitLab-EE - A (potentially) current version of Enterprise EE. Suggest a quick build before using.
* GitLab-Runner-Dedicated-No-Docker - A dedicated EC2 AMI sans Docker.
* OpenLDAP - OpenLDAP AMI w/ sample data (see below)
* Node - Creates Node.js 10.15.0 EC2 AMI
* Redis - **_Redis Is Currently Broken_**


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

If you want to build in your local environment then you will need to install [HashiCorp Packer](https://www.packer.io/downloads.html)

The scripts assume you have exported your AWS programatic credentials. The format for exporting is:

* ``export AWS_ACCESS_KEY_ID="yourkeyid"``
* ``export AWS_SECRET_ACCESS_KEY="yoursecretid"``

### Creating AWS AMIs

Individual local builds follow the format of

``packer build ./<directory>/*.json``

For example, to a build GitLab Enterprise Edition the command would (from root of clone):

``packer build ./GitLab-EE/*.json``



## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Some Site](http://127.0.0.1) - Some site



## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
