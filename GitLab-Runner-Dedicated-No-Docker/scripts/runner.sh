#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
apt update
apt upgrade
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash
cat > /etc/apt/preferences.d/pin-gitlab-runner.pref <<EOF
Explanation: Prefer GitLab provided packages over the Debian native ones
Package: gitlab-runner
Pin: origin packages.gitlab.com
Pin-Priority: 1001
EOF
apt install rand gitlab-runner fastjar
wget -qO- https://releases.hashicorp.com/terraform/0.11.11/terraform_0.11.11_linux_amd64.zip | jar xv
wget -qO- https://releases.hashicorp.com/packer/1.3.3/packer_1.3.3_linux_amd64.zip | jar xv
mv /home/ubuntu/terraform /usr/local/bin/terraform
mv /home/ubuntu/packer /usr/local/bin/packer
chmod +x /usr/local/bin/terraform
chmod +x /usr/local/bin/packer
#
#comment
