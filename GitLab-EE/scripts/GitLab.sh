#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
# external domain name should be under your control.
external_domain = gitlab.kh.sa.gl-demo.io


# echo "postfix postfix/mailname string $external_domain" | debconf-set-selections
# echo "postfix postfix/main_mailer_type string 'Internet Site'" | debconf-set-selections
apt update
apt upgrade -y
#apt install postfix openssh-server ca-certificates ldap-utils
apt install openssh-server ca-certificates
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | bash
EXTERNAL_URL=$external_domain apt install gitlab-ee
